const dynamodb = require('dynopromise-client')

/**
 * Recursively reads chunks of the table until all items of the table are in
 * the pool. When there are no more items to read, the pool of items is
 * returned (unless fullScanParams.noReturn is set).
 * @param {Object} db 
 * @param {Object} scanParams 
 * @param {Object} fullScanParams 
 * @param {Array} pool 
 * @param {String} lastEvaluatedKey 
 */
const doScan = (db, scanParams, fullScanParams, pool = [], lastEvaluatedKey = null) => {
	if (lastEvaluatedKey) {
		scanParams.ExclusiveStartKey = lastEvaluatedKey
	}
	return db.scan(scanParams)
	.then(result => {
		const batch = result.Items
		const batchHasItems = batch && batch.length

		// If there's items and we're not expected to return anything and there's
		// more items to come, then process this chunk of items and recurse to the
		// next chunk of items.
		if (batchHasItems && fullScanParams.noReturn && result.LastEvaluatedKey) {
			return fullScanParams.onChunk(result)
			.then(() => doScan(
				db, scanParams, fullScanParams, pool, result.LastEvaluatedKey
			))
		}

		// If there's items and we're next expected to return anything and there's
		// no more items to come, then process this chunk and return.
		if (batchHasItems && fullScanParams.noReturn) {
			return fullScanParams.onChunk(result)
		}

		// If there's items and we should return the full set and there's more items
		// to come, then add this chunk to the pool of items, process this chunk
		// and recurse to the next chunk.
		if (batchHasItems && result.LastEvaluatedKey) {
			const newPool = pool.concat(batch)
			return fullScanParams.onChunk(result)
			.then(() => doScan(
				db, scanParams, fullScanParams, newPool, result.LastEvaluatedKey
			))
		}

		// If there's items and we should return the full set and there's no more
		// items to come, then add this chunk to the pool of items, process this
		// chunk and return the newPool of items.
		if (batchHasItems) {
			const newPool = pool.concat(batch)
			return fullScanParams.onChunk(result)
			.then(() => newPool)
		}

		// If there's no items and we should return the full set and there's no more
		// items to come, then return the last pool of items.
		return pool
	})
}

/**
 * Does a full scan of the database table, returning a promise which, on 
 * completion, returns all of the items in the table.
 * @param dbOptions {Object} Options passed to AWS.DocumentClient(...)
 * @param scanParams {Object} Options passed to db.scan(...). Optional.
 * @param fullScanParams {Object} Options for this scan in form:
 * {onChunk: chunk => {...}}
 * Optional.
 * @returns {Promise<Array>} Array of all items in db table.
 */
module.exports = (dbOptions, scanParams = {}, fullScanParams = {}) => {
	const db = dynamodb(dbOptions)
	const realFullScanParams = Object.assign(
		{
			noReturn: false,
			onChunk: () => Promise.resolve()
		},
		fullScanParams
	)

	if (typeof realFullScanParams.onChunk !== 'function') {
		throw new Error('fullScanParams.onChunk must be a function that returns a '
		+ 'Promise')
	}

	return doScan(db, scanParams, realFullScanParams)
}
