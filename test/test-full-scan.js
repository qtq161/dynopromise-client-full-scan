require('dotenv').config()
const assert = require('assert')
const AWS = require('aws-sdk')
const dynamodb = require('dynopromise-client')
const uuid = require('uuid/v1')
const fullScan = require('../src/index')
const ProgressBar = require('progress')

const db = dynamodb({
	region: 'us-east-1',
	endpoint: 'http://localhost:8000'
})

const TableName = 'dynopromiseClientFullScanTest25439'

const totalFakeItemsToUse = 50
const batchSize = 25

const createTable = () => new Promise((resolve, reject) => {
	console.log('Creating test table')
	const fullDb = new AWS.DynamoDB({
		region: process.env.TEST_AWS_REGION,
		endpoint: process.env.TEST_AWS_ENDPOINT,
	})

	// Create a table to test on
	fullDb.createTable(
		{
			TableName,
			KeySchema: [
				{
					AttributeName: 'id',
					KeyType: 'HASH',
				}
			],
			AttributeDefinitions: [
				{
					AttributeName: 'id',
					AttributeType: 'S',
				}
			],
			ProvisionedThroughput: {
				ReadCapacityUnits: 5,
				WriteCapacityUnits: 5
			},
		}, err => {
			// Show any errors (except the error stating the table already exists)
			if (err && err.name !== 'ResourceInUseException') {
				reject(err)
			}
			console.log('Test table created')
			resolve()
		}
	)
})

const writeFakeItems = async () => {
	console.log('loading fake items into db')
	let fakeItemCount = totalFakeItemsToUse
	const bar = new ProgressBar(':bar :percent', {
		total: (fakeItemCount / batchSize)
	})
	while (fakeItemCount > 0) {
		await db.batchWrite({
			RequestItems: {
				[TableName]: Array.apply(null, Array(batchSize)).map(() => ({
					PutRequest: {
						Item: {
							id: uuid()
						}
					}
				}))
			}
		})
		bar.tick()
		fakeItemCount -= batchSize
	}
}

const testScan = async () => {
	console.log('testing full scan')
	const bar = new ProgressBar(':bar :percent', {
		total: 100
	})
	let scannedCount = 0
	let chunksHit = 0
	const scanResult = await fullScan(
		{
			region: process.env.TEST_AWS_REGION,
			endpoint: process.env.TEST_AWS_ENDPOINT,
		},
		{ TableName },
		{
			onChunk: result => new Promise((resolve, reject) => {
				if (typeof result.Items === undefined) {
					return reject('result with Items property should passed to chunk')
				}
				chunksHit++
				if (result && result.Items && result.Items.length) {
					scannedCount += result.Items.length
					bar.update(scannedCount / totalFakeItemsToUse)
				}
				resolve()
			})
		}
	)
	assert.strictEqual(scanResult.length, totalFakeItemsToUse)
	console.log('Successfully scanned full table')
	assert.ok(chunksHit > 0)
	console.log('Successfully ran onChunk function')

	const scanResult2 = await fullScan(
		{
			region: process.env.TEST_AWS_REGION,
			endpoint: process.env.TEST_AWS_ENDPOINT,
		},
		{ TableName },
		{
			noReturn: true
		}
	)
	assert.strictEqual(scanResult2, undefined)
	console.log('Successfully tested noReturn parameter')
}

const deleteTable = () => new Promise((resolve, reject) => {
	const fullDb = new AWS.DynamoDB({
		region: process.env.TEST_AWS_REGION,
		endpoint: process.env.TEST_AWS_ENDPOINT,
	})

	fullDb.deleteTable(
		{ TableName },
		err => {
			if (err) {
				return reject(err)
			}
			console.log('Test table deleted')
			resolve()
		}
	)
})

const runTests = async () => {
	try {
		console.log('Note: this test takes a long time (hours).')
		await createTable()
		await writeFakeItems()
		await testScan()
	}
	catch (err) {
		console.error(err)
	}
	finally {
		try {
			await deleteTable()
		}
		catch (err) {
			console.error(err)
		}
	}
}

runTests()
